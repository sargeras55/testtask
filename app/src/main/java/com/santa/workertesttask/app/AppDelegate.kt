package com.santa.workertesttask.app

import android.app.Application
import com.activeandroid.ActiveAndroid
import com.santa.workertesttask.config.Config
import com.santa.workertesttask.dependency.common.CommonComponent
import com.santa.workertesttask.dependency.common.DaggerCommonComponent
import com.santa.workertesttask.dependency.common.NetworkModule


class AppDelegate: Application() {

    lateinit var appComponent: CommonComponent


    override fun onCreate() {
        super.onCreate()
        ActiveAndroid.initialize(this)
        initCommonComponent()
    }


    private fun initCommonComponent() {
        appComponent = DaggerCommonComponent
                .builder()
                .networkModule(NetworkModule(Config.BASE_URL))
                .build()
    }

}