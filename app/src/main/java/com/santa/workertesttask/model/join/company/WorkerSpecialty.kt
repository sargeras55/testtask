package com.santa.workertesttask.model.join.company

import com.activeandroid.Model
import com.activeandroid.annotation.Column
import com.activeandroid.annotation.Table
import com.santa.workertesttask.model.specialty.Specialty
import com.santa.workertesttask.model.worker.Worker

/**
 * Created by santa on 17.12.2017.
 */


/**
 * Join for [Worker] and [Specialty]
 */
@Table(name = "workerspecialty")
data class WorkerSpecialty(

        @Column(name = "worker",
                onDelete = Column.ForeignKeyAction.NO_ACTION)
        val worker: Worker? = null,

        @Column(name = "specialty",
                onDelete = Column.ForeignKeyAction.CASCADE)
        val specialty: Specialty? = null

): Model() {

        companion object {
                const val WORKERSPECIALTY_ID = "Id"
        }

}