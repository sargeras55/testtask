package com.santa.workertesttask.model.specialty

import com.santa.workertesttask.model.common.Repository


interface SpecialtyRepository : Repository<Specialty, Int> {

    fun findSpecialtiesByWorkerId(workerId: Long): List<Specialty>

}