package com.santa.workertesttask.model.common


/**
 * Default repository
 * [Entity] using for Type entity
 * [Id] for main entity identifier
 */
interface Repository<Entity: Any, in Id: Any> {

    /**
     * Save [Entity] inside repository
     * @param entity object to save
     */
    fun save(entity: Entity?)


    /**
     * Save [Collection][Entity] inside repository
     * @param collection collection to save
     */
    fun saveCollection(collection: List<Entity>?)


    /**
     * @return all object from repository
     */
    fun findAll(): List<Entity>


    /**
     * @param id unique identification for object
     * @return optional entity
     */
    fun findById(id: Id?): Entity?


    /**
     * @param entity optional entity for delete
     * Delete specific entity from database
     */
    fun delete(entity: Entity?)


    /**
     * Delete all object from database
     */
    fun deleteAll()


    /**
     * Get database entities count
     */
    fun count(): Int

}