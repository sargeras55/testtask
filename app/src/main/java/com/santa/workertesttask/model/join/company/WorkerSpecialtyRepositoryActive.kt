package com.santa.workertesttask.model.join.company

import com.activeandroid.query.Delete
import com.activeandroid.query.Select
import com.santa.workertesttask.util.database.saveInTransaction


/**
 * Implementation [com.santa.workertesttask.model.common.Repository]
 */
class WorkerSpecialtyRepositoryActive : WorkerSpecialtyRepository {


    override fun save(entity: WorkerSpecialty?) {
        entity?.save()
    }


    override fun saveCollection(collection: List<WorkerSpecialty>?) {
        saveInTransaction { collection?.forEach { it.save() } }
    }


    override fun findAll(): List<WorkerSpecialty> = Select().from(WorkerSpecialty::class.java).execute()


    override fun findById(id: Int?): WorkerSpecialty? = Select().from(WorkerSpecialty::class.java)
            .where("${WorkerSpecialty.WORKERSPECIALTY_ID} = ?", id)
            .executeSingle()


    override fun delete(entity: WorkerSpecialty?) {
        entity?.delete()
    }


    override fun deleteAll() {
        Delete().from(WorkerSpecialty::class.java).execute<WorkerSpecialty>()
    }


    override fun count(): Int = Select().from(WorkerSpecialty::class.java).count()

}