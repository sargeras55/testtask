package com.santa.workertesttask.model.worker

import com.santa.workertesttask.model.common.Repository


interface WorkerRepository: Repository<Worker, Long> {

    fun findBySpecialtyModelId(specialtyId: Long): List<Worker>

}