package com.santa.workertesttask.model.specialty

import android.os.Parcel
import android.os.Parcelable
import com.activeandroid.Model
import com.activeandroid.annotation.Column
import com.activeandroid.annotation.Table
import com.santa.workertesttask.util.standard.createParcel



@Table(name = "specialty")
data class Specialty(

        @Column(name = "specialty_id", unique = true, onUniqueConflict = Column.ConflictAction.IGNORE)
        var specialtyId: Int = 0,

        @Column(name = "name")
        var name: String = ""

) : Model(), Parcelable {

    companion object {
        const val SPECIALTY_ID = "specialty_id"

        @JvmField val CREATOR = createParcel { Specialty(it) } // Not touch
    }

    constructor(source: Parcel) : this(source.readInt(), source.readString())


    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeInt(specialtyId)
        dest?.writeString(name)
    }


    override fun describeContents(): Int = 0


}

