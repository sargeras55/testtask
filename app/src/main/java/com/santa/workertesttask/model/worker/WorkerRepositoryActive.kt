package com.santa.workertesttask.model.worker

import com.activeandroid.query.Delete
import com.activeandroid.query.Select
import com.santa.workertesttask.model.join.company.WorkerSpecialty
import com.santa.workertesttask.util.database.saveInTransaction

class WorkerRepositoryActive: WorkerRepository {


    override fun save(entity: Worker?) {
        entity?.save()
    }


    override fun saveCollection(collection: List<Worker>?) {
        saveInTransaction { collection?.forEach { it.save() } }
    }


    override fun findAll(): List<Worker> = Select().from(Worker::class.java).execute()


    override fun findById(id: Long?): Worker? = Select().from(Worker::class.java)
            .where("${Worker.WORKER_ID} = ?", id)
            .executeSingle()


    override fun delete(entity: Worker?) {
        entity?.delete()
    }


    override fun deleteAll() {
        Delete().from(Worker::class.java).execute<Worker>()
    }


    override fun count(): Int = Select().from(Worker::class.java).count()


    override fun findBySpecialtyModelId(specialtyId: Long): List<Worker> = Select().from(Worker::class.java)
            .innerJoin(WorkerSpecialty::class.java)
            .on("WorkerSpecialty.worker = Worker.id")
            .where("WorkerSpecialty.specialty = ?", specialtyId)
            .execute()


}


