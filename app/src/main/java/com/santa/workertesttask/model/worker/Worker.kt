package com.santa.workertesttask.model.worker

import android.os.Parcel
import android.os.Parcelable
import com.activeandroid.Model
import com.activeandroid.annotation.Column
import com.activeandroid.annotation.Table
import com.santa.workertesttask.util.standard.createParcel
import java.util.*


@Table(name = "worker")
data class Worker(

        @Column(name = "first_name")
        var firstName: String = "",

        @Column(name = "last_name")
        var lastName: String = "",

        @Column(name = "birthday")
        var birthday: Date? = null,

        @Column(name = "photo")
        var avatarUrl: String = "",

        @Column(name = "specialties")
        var specialties: String = "",

        @Column(name = "age")
        var age: Int = 0

) : Model(), Parcelable {

    companion object {
        const val WORKER_ID = "Id" // Default

        @JvmField val CREATOR = createParcel { Worker(it) } // Not touch
    }

    constructor(source: Parcel) : this (
            source.readString(),
            source.readString(),
            source.readSerializable() as Date?,
            source.readString(),
            source.readString(),
            source.readInt()
    )


    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(firstName)
        dest?.writeString(lastName)
        dest?.writeSerializable(birthday)
        dest?.writeString(avatarUrl)
        dest?.writeString(specialties)
        dest?.writeInt(age)
    }


    override fun describeContents(): Int = 0

}

