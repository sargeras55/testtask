package com.santa.workertesttask.model.join.company

import com.santa.workertesttask.model.common.Repository


interface WorkerSpecialtyRepository : Repository<WorkerSpecialty, Int>