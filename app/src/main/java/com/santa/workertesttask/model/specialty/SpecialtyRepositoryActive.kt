package com.santa.workertesttask.model.specialty

import com.activeandroid.query.Delete
import com.activeandroid.query.Select
import com.santa.workertesttask.model.join.company.WorkerSpecialty
import com.santa.workertesttask.util.database.saveInTransaction


class SpecialtyRepositoryActive : SpecialtyRepository {


    override fun save(entity: Specialty?) {
        entity?.save()
    }


    override fun saveCollection(collection: List<Specialty>?) {
        saveInTransaction { collection?.forEach { it.save() } }
    }


    override fun findAll(): List<Specialty> = Select().from(Specialty::class.java).execute()


    override fun findById(id: Int?): Specialty? = Select().from(Specialty::class.java)
            .where("${Specialty.SPECIALTY_ID} = ?", id)
            .executeSingle()


    override fun delete(entity: Specialty?) {
        entity?.delete()
    }


    override fun deleteAll() {
        Delete().from(Specialty::class.java).execute<Specialty>()
    }


    override fun count(): Int = Select().from(Specialty::class.java).count()


    override fun findSpecialtiesByWorkerId(workerId: Long): List<Specialty> = Select().from(Specialty::class.java)
            .innerJoin(WorkerSpecialty::class.java)
            .on("WorkerSpecialty.specialty = Specialty.id")
            .where("WorkerSpecialty.worker = ?", workerId)
            .execute()

}