package com.santa.workertesttask.util.standard

import android.os.Parcel
import android.os.Parcelable


inline fun <T, R> T.guard(onTry: (T) -> R, onCatch: (T) -> R): R {
    return try {
        onTry(this)
    } catch (e: Exception) {
        onCatch(this)
    }
}


inline fun <R> guardWith(onTry: () -> R, onCatch: () -> R): R {
    return try {
        onTry()
    } catch (e: Exception) {
        onCatch()
    }
}


/**
 * Helper class for create CREATOR for [Parcelable] object
 * @example @JvmField val CREATOR = createParcel { Specialty(it) }
 */
inline fun <reified T : Parcelable> createParcel(
        crossinline createFromParcel: (Parcel) -> T?
): Parcelable.Creator<T> = object : Parcelable.Creator<T> {
            override fun createFromParcel(source: Parcel): T? = createFromParcel(source)
            override fun newArray(size: Int): Array<out T?> = arrayOfNulls(size)
        }