package com.santa.workertesttask.util.standard

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.*



object DateHelper {
    const val DATE_FORMAT_YMD = "yyyy-MM-dd" // Example: 1932-02-12
    const val DATE_FORMAT_DMY = "dd-MM-yyyy" // Example: 1932-02-12
    const val DATE_FORMAT_DMY_POINT = "dd.MM.yyyy" // Example: 02.12.1932
}


/**
 * Extension for [Date]
 * Calculate [Int] count of year by two date
 *
 * @important Accuracy up to days
 * @example 02.12.1932 - 02.12.1936 will return 4
 *
 * @return [Int] count of year accuracy up to days
 */
fun Date.getDifferenceInYear(lastDate: Date): Int {
    val calendarFrom = getCalendar(Locale.getDefault())
    val calendarTo = lastDate.getCalendar(Locale.getDefault())
    var different = calendarTo.get(YEAR) - calendarFrom.get(YEAR)
    if (calendarFrom.get(MONTH) > calendarTo.get(MONTH)
            || calendarFrom.get(MONTH) == calendarTo.get(MONTH)
            && calendarFrom.get(DATE) > calendarTo.get(DATE)) {
        different--
    }
    return different
}


/**
 * Extension for [Date]
 * @param locale need for right creating [Calendar]
 * @return calendar by input date
 */
private fun Date.getCalendar(locale: Locale): Calendar {
    val cal = Calendar.getInstance(locale)
    cal.time = this
    return cal
}


/**
 * Transform simple dateLabel to [Date] through [formats]
 * @param dateLabel text with old text format
 * @param formats varargs to [SimpleDateFormat]
 *
 * @example dateLabel - 1993.03.29 | formats = yyyy.MM.dd, dd.MM.yyy
 * @return  [Date] with format see @example yyyy.MM.dd
 */
fun transformDateFormat(dateLabel: String, vararg formats: SimpleDateFormat): Date? {
    var date: Date? = null
    formats.forEach {
        try {
            it.isLenient = false
            date = it.parse(dateLabel)
            return date
        } catch (error: ParseException) {
            // Do nothing
        }
    }
    return date
}
