package com.santa.workertesttask.util.helper


object Fonts {

    const val ROBOTO_THIN = "Roboto-Thin.ttf"
    const val ROBOTO_LIGHT = "Roboto-Light.ttf"
    const val ROBOTO_MEDIUM = "Roboto-Medium.ttf"
    const val ROBOTO_REGULAR = "Roboto-Regular.ttf"

}

