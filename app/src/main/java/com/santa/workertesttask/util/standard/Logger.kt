package com.santa.workertesttask.util.standard

import android.util.Log


object Logger {

    fun debug(tag: String, message: String) {
        Log.d(tag, "Logos $message")
    }

}