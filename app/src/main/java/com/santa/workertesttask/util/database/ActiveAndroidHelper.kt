package com.santa.workertesttask.util.database

import com.activeandroid.ActiveAndroid


/**
 * Execute closure inside transaction
 * @param transaction - you should save data inside this closure
 */
inline fun saveInTransaction(transaction: () -> Unit) {
    ActiveAndroid.beginTransaction()
    try {
        transaction()
        ActiveAndroid.setTransactionSuccessful()
    } finally {
        ActiveAndroid.endTransaction()
    }
}



