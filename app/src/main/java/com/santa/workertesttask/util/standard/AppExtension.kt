package com.santa.workertesttask.util.standard

import android.app.Activity
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.santa.workertesttask.app.AppDelegate

/**
 * Get [Activity] from [AppCompatActivity] without reduction
 */
fun Activity.delegate(): AppDelegate = application as AppDelegate


/**
 * Get [AppDelegate] from [Fragment] without reduction
 */
fun Fragment.delegate(): AppDelegate = activity.application as AppDelegate


fun Fragment.setTitle(title: String, withArrow: Boolean = false) {
    val actionBar = (activity as AppCompatActivity).supportActionBar

    actionBar?.let {
        it.title = title
        it.setDisplayHomeAsUpEnabled(withArrow)
        it.setDisplayShowHomeEnabled(withArrow)
    }
}