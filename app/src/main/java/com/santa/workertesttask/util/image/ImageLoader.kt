package com.santa.workertesttask.util.image

import android.support.annotation.DrawableRes
import android.widget.ImageView
import com.santa.workertesttask.util.image.ImageLoader.ImageRequest


/**
 * Abstract for loader image
 * TODO REMAKE SO THAT THE [ImageLoader] WAS ACCEPTED BY THE [ImageRequest] AND PERFORMED IT
 * TODO NOW NEED CREATE IMAGE LOADER ON EACH CUSTOM IMAGE
 */
interface ImageLoader {

    fun loadImage(@DrawableRes redId: Int): ImageRequest
    fun loadImage(url: String): ImageRequest

    interface ImageRequest {
        fun apply(imageView: ImageView, @DrawableRes placeholder: Int)
    }

}


