package com.santa.workertesttask.util.image

import android.support.annotation.DrawableRes
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions


/**
 * Glide implementation for [ImageLoader]
 */
class GlideImageLoader: ImageLoader {

    override fun loadImage(@DrawableRes redId: Int): ImageLoader.ImageRequest = GlideCircleImageResRequest(redId)

    override fun loadImage(url: String): ImageLoader.ImageRequest = GlideCircleImageUrlRequest(url)


    class GlideCircleImageUrlRequest(private val url: String): ImageLoader.ImageRequest {
        override fun apply(imageView: ImageView, placeholder: Int) {
            Glide.with(imageView)
                    .load(url)
                    .apply(RequestOptions.circleCropTransform())
                    .apply(RequestOptions.placeholderOf(placeholder).circleCrop())
                    .into(imageView)
        }
    }


    class GlideCircleImageResRequest(@DrawableRes private val resId: Int): ImageLoader.ImageRequest {
        override fun apply(imageView: ImageView, placeholder: Int) {
            Glide.with(imageView)
                    .load(resId)
                    .apply(RequestOptions.circleCropTransform())
                    .apply(RequestOptions.placeholderOf(placeholder).circleCrop())
                    .into(imageView)
        }
    }

}
