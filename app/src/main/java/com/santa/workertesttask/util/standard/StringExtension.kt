package com.santa.workertesttask.util.standard


object StringHelper {
    const val EMPTY = ""
    const val HYPHEN = "-"
}


/**
 * Extension for standard function
 * Standard [String.capitalize] not make charters with index > 0 to lower case
 *
 * @example andrew -> Andrew | aNDREW -> Andrew | ANDREW -> Andrew
 *
 * @return string with first index char upper case and other in lower
 */
fun String.capitalizeWithLower(): String = if (isNotEmpty())
    substring(0, 1).toUpperCase() + substring(1).toLowerCase() else this