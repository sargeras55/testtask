package com.santa.workertesttask.util.standard

import android.graphics.Typeface
import android.widget.TextView

/**
 * Set typeface to [TextView] by [String] [typefaceName]
 * Not overhead on many count invoke (˜10ms on 100 [TextView])
 */
fun TextView.applyTypeface(typefaceName: String) {
    typeface = Typeface.createFromAsset(context.assets, typefaceName)
}
