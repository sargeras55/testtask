package com.santa.workertesttask.network.adapter.worker

import com.santa.workertesttask.model.join.company.WorkerSpecialty
import com.santa.workertesttask.model.specialty.Specialty
import com.santa.workertesttask.model.worker.Worker


data class WorkersWrapper(
        val workers: List<Worker>,
        val specialty: List<Specialty>,
        val workerSpecialty: List<WorkerSpecialty>
)
