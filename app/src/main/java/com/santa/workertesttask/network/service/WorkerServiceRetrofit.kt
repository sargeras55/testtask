package com.santa.workertesttask.network.service


import com.santa.workertesttask.network.adapter.worker.WorkersWrapper
import com.santa.workertesttask.network.api.SixtyFiveApi
import io.reactivex.Single
import javax.inject.Inject


class WorkerServiceRetrofit @Inject constructor(private val sixtyFiveApi: SixtyFiveApi): WorkerService {


    override fun loadWorkers(): Single<WorkersWrapper> = sixtyFiveApi
            .getWorkers()
            .flatMap { Single.fromCallable {it.workers} }

}