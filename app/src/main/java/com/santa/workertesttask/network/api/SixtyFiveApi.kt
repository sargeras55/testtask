package com.santa.workertesttask.network.api

import com.santa.workertesttask.network.adapter.worker.WorkersResponse
import io.reactivex.Single
import retrofit2.http.GET


interface SixtyFiveApi {

    @GET("65gb/static/raw/master//testTask.json")
    fun getWorkers(): Single<WorkersResponse>

}