package com.santa.workertesttask.network.adapter.worker

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.santa.workertesttask.model.join.company.WorkerSpecialty
import com.santa.workertesttask.model.specialty.Specialty
import com.santa.workertesttask.model.worker.Worker
import com.santa.workertesttask.util.standard.*
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.*


class WorkerRequestWrapperAdapter: JsonDeserializer<WorkersWrapper> {

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): WorkersWrapper {
        val workers = mutableListOf<Worker>()
        val specialties = mutableSetOf<Specialty>()
        val workerSpecialty = mutableListOf<WorkerSpecialty>()

        val dateFormatYmdHyphen = SimpleDateFormat(DateHelper.DATE_FORMAT_YMD, Locale.getDefault())
        val dateFormatDmyHyphen = SimpleDateFormat(DateHelper.DATE_FORMAT_DMY, Locale.getDefault())

        json?.asJsonArray?.forEach {
            val jsonWorker = it.asJsonObject

            // Parse Worker
            val worker = Worker()
            worker.firstName = jsonWorker.get("f_name").asString.capitalizeWithLower()
            worker.lastName = jsonWorker.get("l_name").asString.capitalizeWithLower()
            worker.avatarUrl = jsonWorker.get("avatr_url").guard(onTry = {it.asString}, onCatch = {StringHelper.EMPTY})

            val birthday = jsonWorker.get("birthday").guard(onTry = {it.asString}, onCatch = {StringHelper.EMPTY})
            if (birthday.isNotEmpty()) {
                worker.birthday = transformDateFormat(birthday, dateFormatYmdHyphen, dateFormatDmyHyphen)
                worker.age = worker.birthday?.run { getDifferenceInYear(Date()) } ?: 0
            }
            workers.add(worker)

            // Parse specialty
            val specialtiesArray = jsonWorker.getAsJsonArray("specialty")
            specialtiesArray.forEachIndexed { index, json ->
                val jsonSpecialty = json.asJsonObject

                var specialty = Specialty()
                specialty.specialtyId = jsonSpecialty.get("specialty_id").asInt
                specialty.name = jsonSpecialty.get("name").asString
                if (index == specialtiesArray.count() - 1)
                    worker.specialties += specialty.name
                else
                    worker.specialties += "${specialty.name} ,"
                if (!specialties.contains(specialty))
                    specialties.add(specialty)
                else
                    specialties.forEach { if (it.specialtyId == specialty.specialtyId) specialty = it } // TODO NEED CHANGE THIS GOVNOCODE

                // Add worker speciality
                workerSpecialty.add(WorkerSpecialty(worker, specialty))
            }
        }

        return WorkersWrapper(workers, specialties.toList(), workerSpecialty)
    }

}

