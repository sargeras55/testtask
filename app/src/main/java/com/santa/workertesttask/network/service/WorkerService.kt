package com.santa.workertesttask.network.service


import com.santa.workertesttask.network.adapter.worker.WorkersWrapper
import io.reactivex.Single


interface WorkerService {

    fun loadWorkers(): Single<WorkersWrapper>

}