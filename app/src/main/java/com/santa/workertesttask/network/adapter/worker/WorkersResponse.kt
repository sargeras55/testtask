package com.santa.workertesttask.network.adapter.worker

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class WorkersResponse(
        @SerializedName("response")
        @Expose
        val workers: WorkersWrapper
)