package com.santa.workertesttask.module.workers.contract

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.santa.workertesttask.adapter.worker.WorkerAdapter
import com.santa.workertesttask.dependency.workers.DaggerWorkersComponent
import com.santa.workertesttask.dependency.workers.WorkersModule
import com.santa.workertesttask.model.specialty.Specialty
import com.santa.workertesttask.model.worker.Worker
import com.santa.workertesttask.module.common.view.base.CommonFragment
import com.santa.workertesttask.module.common.view.base.Injectable
import com.santa.workertesttask.module.specialty.contract.SpecialtyFragment
import com.santa.workertesttask.util.image.ImageLoader
import com.santa.workertesttask.util.standard.setTitle
import com.testtask.santa.workertesttask.R
import kotlinx.android.synthetic.main.fragment_workers.*
import javax.inject.Inject


class WorkersFragment: CommonFragment(), WorkersView, Injectable {

    companion object {
        const val EXTRA_WORKER = "EXTRA_WORKER"
    }


    @Inject lateinit var workersAction: WorkersAction
    @Inject lateinit var imageLoader: ImageLoader

    lateinit private var mWorkerAdapter: WorkerAdapter
    override val contentId: Int = R.layout.fragment_workers


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
    }


    override fun inject() {
        val containerId: Int = arguments.getInt(EXTRA_CONTAINER_ID)

        val specialtyComponent = DaggerWorkersComponent.builder()
                .workersModule(WorkersModule(this, containerId, activity))
                .build()

        specialtyComponent.inject(this)
    }


    override fun prepare() {
        val specialty = arguments.getParcelable<Specialty>(SpecialtyFragment.EXTRA_SPECIALTY)
        setTitle(specialty.name, true)
        mRvWorkers.layoutManager = LinearLayoutManager(context)
        workersAction.onTaleWorkersAction(specialty)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        workersAction.onDetachAction()
    }


    override fun showWorkers(workers: List<Worker>) {
        mWorkerAdapter = WorkerAdapter(workers, imageLoader)
        mWorkerAdapter.onClickAction = { worker ->
           workersAction.onShowWorkerAction(worker)
        }

        mRvWorkers.adapter = mWorkerAdapter
    }


    override fun showMessage(resId: Int) {
        messageWith(resId)
    }


}