package com.santa.workertesttask.module.common.view.base

import android.support.annotation.StringRes


/**
 * Interface for class witch notify user through displaying a message on screen
 */
interface Messenger {

    fun messageWith(@StringRes resId: Int)

}