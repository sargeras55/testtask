package com.santa.workertesttask.module.workers.interactor

import com.santa.workertesttask.model.specialty.SpecialtyRepository
import com.santa.workertesttask.model.worker.Worker
import com.santa.workertesttask.model.worker.WorkerRepository
import io.reactivex.Single
import javax.inject.Inject


class WorkersInteractorReactive @Inject constructor(
        val workerRepository: WorkerRepository,
        val specialtyRepository: SpecialtyRepository
): WorkersInteractor {

    override fun findWorkersBySpecialtyId(specialtyId: Int): Single<List<Worker>> = Single.fromCallable {
        val specialty = specialtyRepository.findById(specialtyId)
        workerRepository.findBySpecialtyModelId(specialty!!.id)
    }


}