package com.santa.workertesttask.module.specialty.interactor

import com.santa.workertesttask.model.specialty.Specialty
import io.reactivex.Single


interface SpecialtyInteractor {

    fun findSpecialties(): Single<List<Specialty>>
    fun updateSpecialties(): Single<List<Specialty>>

}