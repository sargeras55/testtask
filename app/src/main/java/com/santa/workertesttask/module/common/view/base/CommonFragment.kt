package com.santa.workertesttask.module.common.view.base

import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast


abstract class CommonFragment: Fragment(), Messenger {

    companion object {
        const val EXTRA_CONTAINER_ID = "EXTRA_CONTAINER_ID"
    }


    abstract val contentId: Int


    abstract fun prepare()


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(contentId, container, false)
    }


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prepare()
    }


    override fun messageWith(@StringRes resId: Int) {
        Toast.makeText(context, getString(resId), Toast.LENGTH_SHORT).show()
    }

}

