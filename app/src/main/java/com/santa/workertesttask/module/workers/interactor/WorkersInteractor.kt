package com.santa.workertesttask.module.workers.interactor

import com.santa.workertesttask.model.worker.Worker
import io.reactivex.Single

interface WorkersInteractor {

    fun findWorkersBySpecialtyId(specialtyId: Int): Single<List<Worker>>

}