package com.santa.workertesttask.module.specialty.router

import com.santa.workertesttask.model.specialty.Specialty
import com.santa.workertesttask.module.common.router.Router


interface SpecialtyRouter: Router {

    fun navigateToWorkers(specialty: Specialty)

    fun navigateToEmpty()

    fun navigateToListSpecialties()

    fun navigateToNetworkError()

    fun navigateToLoading()

}