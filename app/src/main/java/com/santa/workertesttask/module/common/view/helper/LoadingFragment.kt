package com.santa.workertesttask.module.common.view.helper

import com.santa.workertesttask.module.common.view.base.CommonFragment
import com.santa.workertesttask.util.helper.Fonts
import com.santa.workertesttask.util.standard.applyTypeface
import com.testtask.santa.workertesttask.R
import kotlinx.android.synthetic.main.fragment_loading.*


class LoadingFragment: CommonFragment() {

    override val contentId: Int = R.layout.fragment_loading

    override fun prepare() {
        mTvLoadingTitle.applyTypeface(Fonts.ROBOTO_LIGHT)
    }

}