package com.santa.workertesttask.module.common.view.base

import android.os.Bundle
import android.support.annotation.IdRes
import android.support.annotation.StringRes
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.santa.workertesttask.util.standard.Logger


abstract class CommonActivity: AppCompatActivity(), Messenger {

    companion object {
        private val TAG = CommonActivity::class.java.name
    }


    // First priority
    open var contentId: Int = android.R.id.content
    // Second priority
    open var rootFragment: () -> CommonFragment = { throw IllegalStateException("Fragment is not initialize") }
    private lateinit var fragment: CommonFragment


    abstract fun prepareView(savedInstanceState: Bundle?)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (contentId != android.R.id.content) {
            setContentView(contentId)
        }
        else {
            if (savedInstanceState == null) {
                fragment = rootFragment()
                setFragmentContent(fragment)
            }
        }
        prepareView(savedInstanceState)
    }


    fun setFragmentContent(fragment: CommonFragment, @IdRes containerId: Int = android.R.id.content,
                           tag: String? = fragment::class.java.simpleName) {
        Logger.debug(TAG, "setFragmentContent : $tag")
        supportFragmentManager
                .beginTransaction()
                .replace(containerId, fragment, tag)
                .addToBackStack(tag)
                .commit()
    }


    override fun messageWith(@StringRes resId: Int) {
        Toast.makeText(this, getString(resId), Toast.LENGTH_SHORT).show()
    }

}