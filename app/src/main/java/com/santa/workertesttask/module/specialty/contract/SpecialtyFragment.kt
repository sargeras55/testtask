package com.santa.workertesttask.module.specialty.contract

import android.os.Bundle
import android.support.annotation.IdRes
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import com.santa.workertesttask.adapter.specialty.SpecialtyAdapter
import com.santa.workertesttask.dependency.specialty.DaggerSpecialtyComponent
import com.santa.workertesttask.dependency.specialty.SpecialtyModule
import com.santa.workertesttask.model.specialty.Specialty
import com.santa.workertesttask.module.common.view.base.CommonFragment
import com.santa.workertesttask.module.common.view.base.Injectable
import com.santa.workertesttask.util.image.ImageLoader
import com.santa.workertesttask.util.standard.delegate
import com.santa.workertesttask.util.standard.setTitle
import com.testtask.santa.workertesttask.R
import kotlinx.android.synthetic.main.fragment_specialty.*
import javax.inject.Inject


class SpecialtyFragment: CommonFragment(), SpecialtyView, Injectable {

    companion object {
        const val EXTRA_SPECIALTY = "EXTRA_SPECIALTY"

        fun getInstance(@IdRes containerId: Int): SpecialtyFragment {
            val fragment = SpecialtyFragment()
            val args = Bundle()

            args.putInt(CommonFragment.EXTRA_CONTAINER_ID, containerId)
            fragment.arguments = args

            return fragment
        }
    }


    @Inject lateinit var specialtyAction: SpecialtyActionListener
    @Inject lateinit var imageLoader: ImageLoader

    private lateinit var specialtyAdapter: SpecialtyAdapter

    override val contentId: Int = R.layout.fragment_specialty


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
    }


    override fun inject() {
        val containerId: Int = arguments.getInt(EXTRA_CONTAINER_ID)

        val specialtyComponent = DaggerSpecialtyComponent.builder()
                .commonComponent(delegate().appComponent)
                .specialtyModule(SpecialtyModule(this, containerId, activity))
                .build()

        specialtyComponent.inject(this)
    }


    override fun prepare() {
        setTitle(getString(R.string.specialties))
        mRvSpecialties.layoutManager = LinearLayoutManager(context)
        mRvSpecialties.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        mRvSpecialties.itemAnimator = DefaultItemAnimator()

        specialtyAction.onTakeSpecialtiesAction()
        mSrlSpecialtyUpdater.setOnRefreshListener {
            specialtyAction.onRefreshSpecialtiesAction()
        }
    }


    override fun showSpecialties(specialties: List<Specialty>) {
        specialtyAdapter = SpecialtyAdapter(specialties, imageLoader)
        specialtyAdapter.onClickAction = { specialty ->
            specialtyAction.onShowWorkersAction(specialty)
        }
        mRvSpecialties.adapter = specialtyAdapter
    }


    override fun showMessage(resId: Int) {
        messageWith(resId)
    }


    override fun refreshComplete() {
        mSrlSpecialtyUpdater.isRefreshing = false
    }


    override fun onDestroyView() {
        super.onDestroyView()
        specialtyAction.onDetachAction()
    }

}