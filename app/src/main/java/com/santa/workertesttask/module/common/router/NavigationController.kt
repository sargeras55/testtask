package com.santa.workertesttask.module.common.router

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment


typealias Root = Activity
typealias ViewController = Fragment
typealias Args = Bundle


interface NavigationController {

    /**
     * Navigate to first [ViewController] inside Back Stack
     */
    fun popToRootView()


    /**
     * Navigate back on single step
     */
    fun popToBackView()


    /**
     * Navigate back by [ViewController] index
     */
    fun popToView(index: Int)


    /**
     * Navigate back by [ViewController] tag
     */
    fun popToView(tag: String)


    /**
     * Delete all view except first [ViewController] and replace it
     */
    fun setRootView(
            viewController: ViewController,
            args: Args = Bundle(),
            tag: String = viewController.javaClass.simpleName
    )


    /**
     * Add [ViewController] to back stack
     */
    fun presentView(
            viewController: ViewController,
            args: Args = Args(),
            tag: String = viewController.javaClass.simpleName
    )


    /**
     * Change current root [Root] on new by [Class]
     */
    fun changeRoot(
            to: Class<out Root>,
            args: Args = Args()
    )


    /**
     * Close current [Root] and all [ViewController]
     */
    fun dismissRoot()


    /**
     * Present [ViewController] above current [ViewController],
     * current should be alive and not put inside back stack
     *
     * If this method calling double first popUp is replaced on second
     */
    fun presentPopUpView(
            viewController: ViewController,
            args: Args = Args(),
            tag: String = viewController.javaClass.simpleName
    )


    /**
     * Dismiss current popUp if he existing
     */
    fun dismissPopUpView()


    /**
     * Set title to [ViewController] through [Root]
     */
    fun setNavigationTitle(title: String)

}
