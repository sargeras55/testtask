package com.santa.workertesttask.module.specialty.router

import com.santa.workertesttask.model.specialty.Specialty
import com.santa.workertesttask.module.common.router.Args
import com.santa.workertesttask.module.common.router.NavigationController
import com.santa.workertesttask.module.common.view.helper.EmptyListFragment
import com.santa.workertesttask.module.common.view.helper.LoadingFragment
import com.santa.workertesttask.module.common.view.helper.NotConnectionFragment
import com.santa.workertesttask.module.specialty.contract.SpecialtyFragment
import com.santa.workertesttask.module.workers.contract.WorkersFragment


class SpecialtyRouterSupport(override var navigationController: NavigationController) : SpecialtyRouter {


    override fun navigateToWorkers(specialty: Specialty) {
        val arguments = Args()
        arguments.putParcelable(SpecialtyFragment.EXTRA_SPECIALTY, specialty)
        navigationController.presentView(WorkersFragment(), arguments)
    }


    override fun navigateToEmpty() {
        navigationController.presentPopUpView(EmptyListFragment())
    }


    override fun navigateToNetworkError() {
        navigationController.presentPopUpView(NotConnectionFragment())
    }


    override fun navigateToListSpecialties() {
        navigationController.dismissPopUpView()
    }


    override fun navigateToLoading() {
        navigationController.presentPopUpView(LoadingFragment())
    }

}