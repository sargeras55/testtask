package com.santa.workertesttask.module.common.router


/**
 * Abstract [Router] must manage routing within one [NavigationController] [ViewController]
 */
interface Router {

    var navigationController: NavigationController

}