package com.santa.workertesttask.module.specialty.contract

import android.support.annotation.StringRes
import com.santa.workertesttask.model.specialty.Specialty


interface SpecialtyActionListener {

    fun onTakeSpecialtiesAction()
    fun onRefreshSpecialtiesAction()
    fun onDetachAction()

    fun onShowWorkersAction(specialty: Specialty)
    fun onShowEmptyListAction()
    fun onShowListAction()
    fun onShowNetworkErrorAction()

}

interface SpecialtyView {

    fun showMessage(@StringRes resId: Int)
    fun showSpecialties(specialties: List<Specialty>)
    fun refreshComplete()

}