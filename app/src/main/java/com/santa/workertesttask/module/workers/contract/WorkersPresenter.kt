package com.santa.workertesttask.module.workers.contract

import com.santa.workertesttask.model.specialty.Specialty
import com.santa.workertesttask.model.worker.Worker
import com.santa.workertesttask.module.common.presenter.ReactiveAction
import com.santa.workertesttask.module.workers.interactor.WorkersInteractor
import com.santa.workertesttask.module.workers.router.WorkersRouter
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


class WorkersPresenter @Inject constructor(
        private val workersView: WorkersView,
        private val workersRouter: WorkersRouter,
        private val workersInteractor: WorkersInteractor,
        override var disposables: CompositeDisposable = CompositeDisposable()
) : WorkersAction, ReactiveAction {


    override fun onTaleWorkersAction(specialty: Specialty) {
        workersInteractor.findWorkersBySpecialtyId(specialty.specialtyId)
                .bindSubscribe(onSuccess = { workers ->
                    if (workers.isNotEmpty()) {
                        workersRouter.navigateToWorkerList()
                        workersView.showWorkers(workers)
                    } else {
                        workersRouter.navigateToEmpty()
                    }
                }, onError = { error ->
                    error.printStackTrace()
                })
    }


    override fun onShowWorkerAction(worker: Worker) {
        workersRouter.navigateToWorker(worker)
    }


    override fun onDetachAction() {
        clearDisposables()
    }

}