package com.santa.workertesttask.module.workers.contract

import android.support.annotation.StringRes
import com.santa.workertesttask.model.specialty.Specialty
import com.santa.workertesttask.model.worker.Worker


interface WorkersAction {

    fun onTaleWorkersAction(specialty: Specialty)
    fun onDetachAction()
    fun onShowWorkerAction(worker: Worker)

}

interface WorkersView {

    fun showMessage(@StringRes resId: Int)
    fun showWorkers(workers: List<Worker>)

}