package com.santa.workertesttask.module.workers.router

import com.santa.workertesttask.model.worker.Worker
import com.santa.workertesttask.module.common.router.Router

interface WorkersRouter: Router {

    fun navigateToWorker(worker: Worker)

    fun navigateToEmpty()

    fun navigateToWorkerList()

}