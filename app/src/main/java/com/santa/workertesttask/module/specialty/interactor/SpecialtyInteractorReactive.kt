package com.santa.workertesttask.module.specialty.interactor

import com.santa.workertesttask.model.join.company.WorkerSpecialtyRepository
import com.santa.workertesttask.model.specialty.Specialty
import com.santa.workertesttask.model.specialty.SpecialtyRepository
import com.santa.workertesttask.model.worker.WorkerRepository
import com.santa.workertesttask.network.service.WorkerService
import com.santa.workertesttask.util.standard.Logger
import io.reactivex.Single
import javax.inject.Inject


class SpecialtyInteractorReactive @Inject constructor(
        private val specialityRepository: SpecialtyRepository,
        private val workerRepository: WorkerRepository,
        private val companyRepository: WorkerSpecialtyRepository,
        private val workerService: WorkerService
) : SpecialtyInteractor {


    companion object {
        private val TAG = SpecialtyInteractorReactive::class.java.name
    }


    override fun findSpecialties(): Single<List<Specialty>> = Single.fromCallable {
        specialityRepository.findAll()
    }


    override fun updateSpecialties(): Single<List<Specialty>> = workerService
            .loadWorkers()
            .flatMap { workersWrapper ->
                if (specialityRepository.count() > 0) specialityRepository.deleteAll()
                if (workerRepository.count() > 0) workerRepository.deleteAll()
                if (companyRepository.count() > 0) companyRepository.deleteAll()

                Logger.debug(TAG, "Wrapper $workersWrapper")
                workerRepository.saveCollection(workersWrapper.workers)
                specialityRepository.saveCollection(workersWrapper.specialty)
                companyRepository.saveCollection(workersWrapper.workerSpecialty) // Save after references

                Single.fromCallable { specialityRepository.findAll() }
            }


}