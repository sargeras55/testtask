package com.santa.workertesttask.module.common.router

import android.app.Activity
import android.content.Intent
import android.support.annotation.IdRes
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentManager.POP_BACK_STACK_INCLUSIVE
import android.support.v7.app.AppCompatActivity
import com.santa.workertesttask.module.common.view.base.CommonFragment.Companion.EXTRA_CONTAINER_ID
import com.testtask.santa.workertesttask.R


class NavigationControllerSupport(
        private val root: Root,
        @IdRes private val containerId: Int
): NavigationController {


    private var mPopUpCurrentView: ViewController? = null
    private var mFragmentManager: FragmentManager = (root as AppCompatActivity).supportFragmentManager


    override fun popToRootView() {
        mFragmentManager.popBackStack(null, POP_BACK_STACK_INCLUSIVE)
    }


    override fun popToBackView() {
        mFragmentManager.popBackStack()
    }


    override fun popToView(index: Int) {
        mFragmentManager.popBackStack(index, POP_BACK_STACK_INCLUSIVE)
    }


    override fun popToView(tag: String) {
        mFragmentManager.popBackStack(tag, POP_BACK_STACK_INCLUSIVE)
    }


    override fun setRootView(viewController: ViewController, args: Args, tag: String) {
        args.putInt(EXTRA_CONTAINER_ID, containerId)
        viewController.arguments = args
        mFragmentManager.beginTransaction()
                .setCustomAnimations(
                        R.anim.enter_scale,
                        R.anim.exit_scale,
                        R.anim.pop_enter_scale,
                        R.anim.pop_exit_scale
                )
                .replace(containerId, viewController, tag)
                .addToBackStack(tag)
                .commit()
    }


    override fun changeRoot(to: Class<out Activity>, args: Args) {
        args.putInt(EXTRA_CONTAINER_ID, containerId)
        root.startActivity(Intent(root.baseContext, to))
    }


    override fun presentView(viewController: ViewController, args: Args, tag: String) {
        args.putInt(EXTRA_CONTAINER_ID, containerId)
        viewController.arguments = args
        mFragmentManager.beginTransaction()
                .setCustomAnimations(
                        R.anim.enter_scale,
                        R.anim.exit_scale,
                        R.anim.pop_enter_scale,
                        R.anim.pop_exit_scale
                )
                .replace(containerId, viewController, tag)
                .addToBackStack(viewController.javaClass.simpleName)
                .commit()
    }


    override fun presentPopUpView(viewController: ViewController, args: Args, tag: String) {
        args.putInt(EXTRA_CONTAINER_ID, containerId)
        viewController.arguments = args

        dismissPopUpView()

        mFragmentManager.beginTransaction()
                .add(containerId, viewController, tag)
                .commit()
        mPopUpCurrentView = viewController
    }


    override fun dismissPopUpView() {
        mPopUpCurrentView?.let {
            mFragmentManager.beginTransaction()
                    .setCustomAnimations(
                            R.anim.enter_scale,
                            R.anim.exit_scale,
                            R.anim.pop_enter_scale,
                            R.anim.pop_exit_scale
                    )
                    .remove(it)
                    .commit()
        }
    }


    override fun setNavigationTitle(title: String) {
        val actionBar = (root as AppCompatActivity).supportActionBar
        actionBar?.apply {
            this.title = title
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }


    override fun dismissRoot() {
        root.onBackPressed()
    }

}