package com.santa.workertesttask.module.specialty.contract

import com.santa.workertesttask.model.specialty.Specialty
import com.santa.workertesttask.module.common.presenter.ReactiveAction
import com.santa.workertesttask.module.specialty.interactor.SpecialtyInteractor
import com.santa.workertesttask.module.specialty.router.SpecialtyRouter
import com.testtask.santa.workertesttask.R
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


class SpecialtyPresenter  @Inject constructor(
        private val specialtyInteractor: SpecialtyInteractor,
        private val specialtyView: SpecialtyView,
        private val specialtyRouter: SpecialtyRouter,
        override var disposables: CompositeDisposable = CompositeDisposable()
): SpecialtyActionListener, ReactiveAction {

    override fun onTakeSpecialtiesAction() {
        specialtyInteractor
                .findSpecialties()
                .bindSubscribe(onError = { error ->
                    specialtyRouter.navigateToEmpty()
                }, onSuccess = { specialties ->
                    if (specialties.isNotEmpty()) {
                        specialtyRouter.navigateToListSpecialties()
                        specialtyView.showSpecialties(specialties)
                    } else {
                         onRefreshSpecialtiesAction()
                    }
                })
    }


    override fun onRefreshSpecialtiesAction() {
        specialtyRouter.navigateToLoading()
        specialtyInteractor
                .updateSpecialties()
                .bindSubscribe(onError = { error ->
                    specialtyView.refreshComplete()
                    specialtyRouter.navigateToNetworkError()
                }, onSuccess = { specialties ->
                    specialtyView.refreshComplete()
                    if (specialties.isNotEmpty()) {
                        specialtyRouter.navigateToListSpecialties()
                        specialtyView.showSpecialties(specialties)
                        specialtyView.showMessage(R.string.data_success_updated)
                    } else {
                        specialtyRouter.navigateToEmpty()
                    }
                })
    }


    override fun onShowWorkersAction(specialty: Specialty) {
        specialtyRouter.navigateToWorkers(specialty)
    }


    override fun onShowEmptyListAction() {
        specialtyRouter.navigateToEmpty()
    }


    override fun onShowListAction() {
        specialtyRouter.navigateToListSpecialties()
    }


    override fun onShowNetworkErrorAction() {
        specialtyRouter.navigateToNetworkError()
    }


    override fun onDetachAction() {
        clearDisposables()
    }

}