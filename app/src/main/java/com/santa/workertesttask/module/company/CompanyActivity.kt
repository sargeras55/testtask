package com.santa.workertesttask.module.company

import android.os.Bundle
import android.view.MenuItem
import com.santa.workertesttask.module.common.view.base.CommonActivity
import com.santa.workertesttask.module.specialty.contract.SpecialtyFragment
import com.testtask.santa.workertesttask.R
import kotlinx.android.synthetic.main.activity_company.*


class CompanyActivity: CommonActivity() {

    override var contentId: Int = R.layout.activity_company


    override fun prepareView(savedInstanceState: Bundle?) {
        mToolbarCompany.setTitle(R.string.specialties)
        setSupportActionBar(mToolbarCompany)
        if (savedInstanceState == null) {
            val containerId = R.id.mFlMainCompany
            setFragmentContent(SpecialtyFragment.getInstance(containerId), containerId)
        } else {
            if (supportFragmentManager.backStackEntryCount > 1) {
                supportActionBar?.apply {
                    setDisplayHomeAsUpEnabled(true)
                    setDisplayShowHomeEnabled(true)
                }
            }
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onBackPressed() {
        val backStackCount = supportFragmentManager.backStackEntryCount

        if (backStackCount > 1)
            supportFragmentManager.popBackStack()
        else
            finish()
    }

}