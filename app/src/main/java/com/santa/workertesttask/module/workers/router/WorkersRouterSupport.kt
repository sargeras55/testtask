package com.santa.workertesttask.module.workers.router

import com.santa.workertesttask.model.worker.Worker
import com.santa.workertesttask.module.common.router.Args
import com.santa.workertesttask.module.common.router.NavigationController
import com.santa.workertesttask.module.common.view.helper.EmptyListFragment
import com.santa.workertesttask.module.worker.contract.WorkerFragment
import com.santa.workertesttask.module.workers.contract.WorkersFragment


class WorkersRouterSupport(
        override var navigationController: NavigationController
) : WorkersRouter {

    override fun navigateToWorker(worker: Worker) {
        val arguments = Args()
        arguments.putParcelable(WorkersFragment.EXTRA_WORKER, worker)
        navigationController.presentView(WorkerFragment(), arguments)
    }


    override fun navigateToEmpty() {
        navigationController.presentPopUpView(EmptyListFragment())
    }


    override fun navigateToWorkerList() {
        navigationController.dismissPopUpView()
    }

}