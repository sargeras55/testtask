package com.santa.workertesttask.module.common.view.helper

import com.santa.workertesttask.module.common.view.base.CommonFragment
import com.santa.workertesttask.util.helper.Fonts
import com.santa.workertesttask.util.standard.applyTypeface
import com.testtask.santa.workertesttask.R
import kotlinx.android.synthetic.main.fragment_empty_list.*


class EmptyListFragment : CommonFragment() {

    override val contentId: Int = R.layout.fragment_empty_list

    override fun prepare() {
        mTvSpecialtyEmptyMessage.applyTypeface(Fonts.ROBOTO_LIGHT)
    }

}