package com.santa.workertesttask.module.common.view.helper

import com.santa.workertesttask.module.common.view.base.CommonFragment
import com.santa.workertesttask.util.helper.Fonts
import com.santa.workertesttask.util.standard.applyTypeface
import com.testtask.santa.workertesttask.R
import kotlinx.android.synthetic.main.fragment_network_error.*


class NotConnectionFragment: CommonFragment() {

    override val contentId: Int = R.layout.fragment_network_error

    override fun prepare() {
        mTvNotConnectionMessage.applyTypeface(Fonts.ROBOTO_LIGHT)
    }

}