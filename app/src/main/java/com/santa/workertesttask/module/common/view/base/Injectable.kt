package com.santa.workertesttask.module.common.view.base


/**
 * Class implementing this interface must
 * be using dependency injection inside [inject] method
 */
interface Injectable {
    fun inject()
}