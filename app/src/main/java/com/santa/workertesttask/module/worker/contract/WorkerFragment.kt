package com.santa.workertesttask.module.worker.contract

import android.os.Bundle
import com.santa.workertesttask.dependency.worker.DaggerWorkerComponent
import com.santa.workertesttask.dependency.worker.WorkerModule
import com.santa.workertesttask.model.worker.Worker
import com.santa.workertesttask.module.common.view.base.CommonFragment
import com.santa.workertesttask.module.common.view.base.Injectable
import com.santa.workertesttask.module.workers.contract.WorkersFragment
import com.santa.workertesttask.util.helper.Fonts
import com.santa.workertesttask.util.image.ImageLoader
import com.santa.workertesttask.util.standard.*
import com.testtask.santa.workertesttask.R
import kotlinx.android.synthetic.main.fragment_worker.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class WorkerFragment: CommonFragment(), Injectable {

    companion object {
        fun getInstance(): WorkerFragment = WorkerFragment() // TODO
    }


    @Inject lateinit var imageLoader: ImageLoader

    override val contentId: Int = R.layout.fragment_worker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
    }


    override fun inject() {
        // val containerId: Int = arguments.getInt(EXTRA_CONTAINER_ID) - DONT TOUCH

        val workerComponent = DaggerWorkerComponent
                .builder()
                .workerModule(WorkerModule())
                .build()

        workerComponent.inject(this)
    }


    override fun prepare() {
        val worker = arguments.getParcelable<Worker>(WorkersFragment.EXTRA_WORKER)
        setTitle("${worker.firstName} ${worker.lastName}", true)

        // Setting name
        val fullName = "${worker.firstName} ${worker.lastName}"
        mTvWorkerName.text = fullName
        mTvWorkerName.applyTypeface(Fonts.ROBOTO_REGULAR)

        // Setting age
        mTvWorkerAge.applyTypeface(Fonts.ROBOTO_LIGHT)
        if (worker.age > 0)
            mTvWorkerAge.text = context.resources.getQuantityString(R.plurals.year, worker.age, worker.age)
        else
            mTvWorkerAge.text = StringHelper.HYPHEN

        // Setting birthday
        val dateFormat = SimpleDateFormat(DateHelper.DATE_FORMAT_DMY_POINT, Locale.getDefault())
        mTvWorkerBirthday.text = guardWith(onTry = { dateFormat.format(worker.birthday) }, onCatch = { StringHelper.HYPHEN })
        mTvWorkerBirthday.applyTypeface(Fonts.ROBOTO_LIGHT)

        // Setting specialty
        mTvWorkerSpecialty.text = worker.specialties
        mTvWorkerSpecialty.applyTypeface(Fonts.ROBOTO_LIGHT)

        // Setting image
        imageLoader.loadImage(worker.avatarUrl)
                .apply(mIvWorkerPhoto, R.drawable.placeholder_face)
    }


    override fun onDestroyView() {
        super.onDestroyView()
    }


}