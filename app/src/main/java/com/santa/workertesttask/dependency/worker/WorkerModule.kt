package com.santa.workertesttask.dependency.worker

import com.santa.workertesttask.util.image.GlideImageLoader
import com.santa.workertesttask.util.image.ImageLoader
import dagger.Module
import dagger.Provides


@Module
class WorkerModule() {

    @Provides
    @WorkerScope
    fun provideImageLoader(): ImageLoader = GlideImageLoader()


}
