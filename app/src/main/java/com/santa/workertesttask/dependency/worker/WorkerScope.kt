package com.santa.workertesttask.dependency.worker

import javax.inject.Scope


@Scope
@MustBeDocumented
@Retention(value = AnnotationRetention.RUNTIME)
annotation class WorkerScope