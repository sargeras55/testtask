package com.santa.workertesttask.dependency.worker

import com.santa.workertesttask.module.worker.contract.WorkerFragment
import dagger.Component


@WorkerScope
@Component(modules = [(WorkerModule::class)])
interface WorkerComponent {
    fun inject(workerFragment: WorkerFragment)
}