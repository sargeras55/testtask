package com.santa.workertesttask.dependency.specialty

import com.santa.workertesttask.dependency.common.CommonComponent
import com.santa.workertesttask.module.specialty.contract.SpecialtyFragment
import dagger.Component


@SpecialtyScope
@Component(dependencies = [(CommonComponent::class)], modules = [(SpecialtyModule::class)])
interface SpecialtyComponent {
    fun inject(specialtyFragment: SpecialtyFragment)
}