package com.santa.workertesttask.dependency.workers

import com.santa.workertesttask.module.workers.contract.WorkersFragment
import dagger.Component


@WorkersScope
@Component(modules = arrayOf(WorkersModule::class))
interface WorkersComponent {
    fun inject(workersFragment: WorkersFragment)
}