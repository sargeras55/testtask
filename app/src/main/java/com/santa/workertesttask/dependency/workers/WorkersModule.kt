package com.santa.workertesttask.dependency.workers

import android.support.annotation.IdRes
import com.santa.workertesttask.model.specialty.SpecialtyRepository
import com.santa.workertesttask.model.specialty.SpecialtyRepositoryActive
import com.santa.workertesttask.model.worker.WorkerRepository
import com.santa.workertesttask.model.worker.WorkerRepositoryActive
import com.santa.workertesttask.module.common.router.NavigationController
import com.santa.workertesttask.module.common.router.NavigationControllerSupport
import com.santa.workertesttask.module.common.router.Root
import com.santa.workertesttask.module.workers.contract.WorkersAction
import com.santa.workertesttask.module.workers.contract.WorkersPresenter
import com.santa.workertesttask.module.workers.contract.WorkersView
import com.santa.workertesttask.module.workers.interactor.WorkersInteractor
import com.santa.workertesttask.module.workers.interactor.WorkersInteractorReactive
import com.santa.workertesttask.module.workers.router.WorkersRouter
import com.santa.workertesttask.module.workers.router.WorkersRouterSupport
import com.santa.workertesttask.util.image.GlideImageLoader
import com.santa.workertesttask.util.image.ImageLoader
import dagger.Module
import dagger.Provides


@Module
class WorkersModule(private val workersView: WorkersView,
                    @IdRes private val containerId: Int,
                    private val root: Root
) {

    @Provides
    @WorkersScope
    fun provideWorkerRepository(): WorkerRepository = WorkerRepositoryActive()



    @Provides
    @WorkersScope
    fun provideSpecialtyRepository(): SpecialtyRepository = SpecialtyRepositoryActive()


    @Provides
    @WorkersScope
    fun provideWorkersInteractor(
            workerRepository: WorkerRepository,
            specialtyRepository: SpecialtyRepository
    ): WorkersInteractor = WorkersInteractorReactive(workerRepository, specialtyRepository)



    @Provides
    @WorkersScope
    fun provideImageLoader(): ImageLoader = GlideImageLoader()



    @Provides
    @WorkersScope
    fun provideNavigationController(): NavigationController = NavigationControllerSupport(root, containerId)


    @Provides
    @WorkersScope
    fun provideWorkesrRoute(navigationController: NavigationController): WorkersRouter = WorkersRouterSupport(navigationController)



    @Provides
    @WorkersScope
    fun providedWorkersAction(workersInteractor: WorkersInteractor, workersRouter: WorkersRouter): WorkersAction =
            WorkersPresenter(workersView = workersView, workersRouter = workersRouter, workersInteractor = workersInteractor)

}
