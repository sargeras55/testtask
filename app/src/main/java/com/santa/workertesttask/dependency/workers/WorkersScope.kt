package com.santa.workertesttask.dependency.workers

import javax.inject.Scope


@Scope
@MustBeDocumented
@Retention(value = AnnotationRetention.RUNTIME)
annotation class WorkersScope