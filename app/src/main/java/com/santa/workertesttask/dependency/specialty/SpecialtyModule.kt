package com.santa.workertesttask.dependency.specialty

import android.support.annotation.IdRes
import com.santa.workertesttask.model.join.company.WorkerSpecialtyRepository
import com.santa.workertesttask.model.join.company.WorkerSpecialtyRepositoryActive
import com.santa.workertesttask.model.specialty.SpecialtyRepository
import com.santa.workertesttask.model.specialty.SpecialtyRepositoryActive
import com.santa.workertesttask.model.worker.WorkerRepository
import com.santa.workertesttask.model.worker.WorkerRepositoryActive
import com.santa.workertesttask.module.common.router.NavigationController
import com.santa.workertesttask.module.common.router.NavigationControllerSupport
import com.santa.workertesttask.module.common.router.Root
import com.santa.workertesttask.module.specialty.contract.SpecialtyActionListener
import com.santa.workertesttask.module.specialty.contract.SpecialtyPresenter
import com.santa.workertesttask.module.specialty.contract.SpecialtyView
import com.santa.workertesttask.module.specialty.interactor.SpecialtyInteractor
import com.santa.workertesttask.module.specialty.interactor.SpecialtyInteractorReactive
import com.santa.workertesttask.module.specialty.router.SpecialtyRouter
import com.santa.workertesttask.module.specialty.router.SpecialtyRouterSupport
import com.santa.workertesttask.network.api.SixtyFiveApi
import com.santa.workertesttask.network.service.WorkerService
import com.santa.workertesttask.network.service.WorkerServiceRetrofit
import com.santa.workertesttask.util.image.GlideImageLoader
import com.santa.workertesttask.util.image.ImageLoader
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit


@Module
class SpecialtyModule(
        private val specialtyView: SpecialtyView,
        @IdRes private val containerId: Int,
        private val root: Root
) {


    @Provides
    @SpecialtyScope
    fun provideApi(retrofit: Retrofit): SixtyFiveApi = retrofit.create(SixtyFiveApi::class.java)


    @Provides
    @SpecialtyScope
    fun provideWorkerService(sixtyFiveApi: SixtyFiveApi): WorkerService = WorkerServiceRetrofit(sixtyFiveApi)


    @Provides
    @SpecialtyScope
    fun provideWorkerRepository(): WorkerRepository = WorkerRepositoryActive()


    @Provides
    @SpecialtyScope
    fun provideCompanyRepository(): WorkerSpecialtyRepository = WorkerSpecialtyRepositoryActive()


    @Provides
    @SpecialtyScope
    fun provideSpecialityRepository(): SpecialtyRepository = SpecialtyRepositoryActive()


    @Provides
    @SpecialtyScope
    fun provideNavigationController(): NavigationController = NavigationControllerSupport(root, containerId)


    @Provides
    @SpecialtyScope
    fun provideRouter(navigationController: NavigationController): SpecialtyRouter = SpecialtyRouterSupport(navigationController)


    @Provides
    @SpecialtyScope
    fun provideImageLoader(): ImageLoader = GlideImageLoader()


    @Provides
    @SpecialtyScope
    fun provideSpecialtyInteractor(
            workerRepository: WorkerRepository,
            specialtyRepository: SpecialtyRepository,
            companyRepository: WorkerSpecialtyRepository,
            workerService: WorkerService
    ): SpecialtyInteractor {

       return SpecialtyInteractorReactive(
               specialityRepository = specialtyRepository,
               workerRepository = workerRepository,
               companyRepository = companyRepository,
               workerService = workerService)

    }


    @Provides
    @SpecialtyScope
    fun provideSpecialtyAction(
            specialtyInteractor: SpecialtyInteractor,
            specialtyRouter: SpecialtyRouter
    ): SpecialtyActionListener {

        return SpecialtyPresenter(
                specialtyInteractor = specialtyInteractor,
                specialtyRouter = specialtyRouter,
                specialtyView = specialtyView)

    }

}