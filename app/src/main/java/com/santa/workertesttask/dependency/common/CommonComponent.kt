package com.santa.workertesttask.dependency.common

import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton


@Singleton
@Component(modules = [(AppModule::class), (NetworkModule::class)])
interface CommonComponent {
    fun dependRetrofit(): Retrofit
}