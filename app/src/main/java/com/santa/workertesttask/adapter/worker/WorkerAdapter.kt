package com.santa.workertesttask.adapter.worker

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.santa.workertesttask.model.worker.Worker
import com.santa.workertesttask.util.helper.Fonts
import com.santa.workertesttask.util.image.ImageLoader
import com.santa.workertesttask.util.standard.applyTypeface
import com.testtask.santa.workertesttask.R
import kotlinx.android.synthetic.main.item_worker.view.*


class WorkerAdapter (
        listWorker: List<Worker>,
        private val imageLoader: ImageLoader
): RecyclerView.Adapter<WorkerViewHolder>() {

    private var mListWorkers: List<Worker> = listWorker

    var onClickAction: (Worker) -> Unit = { }


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): WorkerViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_worker, parent, false)
        return WorkerViewHolder(view, imageLoader)
    }

    override fun getItemCount(): Int = mListWorkers.count()


    override fun onBindViewHolder(holder: WorkerViewHolder?, position: Int) {
        holder?.bindView(mListWorkers[position], onClickAction)
    }

}


class WorkerViewHolder(
        itemView: View,
        private val imageLoader: ImageLoader
): RecyclerView.ViewHolder(itemView) {

    fun bindView(worker: Worker, onClick: (Worker) -> Unit = {}) {
        val context = itemView.context

        // Setting worker name
        val workerFullName = "${worker.firstName} ${worker.lastName}"
        itemView.mTvWorkerItemName.text = workerFullName
        itemView.mTvWorkerItemName.applyTypeface(Fonts.ROBOTO_REGULAR)

        // Setting worker age
        if (worker.age > 0) {
            val workerAge = context.resources.getQuantityString(R.plurals.year, worker.age, worker.age)
            itemView.mTvWorkerItemAge.text = workerAge
            itemView.mTvWorkerItemAge.applyTypeface(Fonts.ROBOTO_LIGHT)
        } else {
            itemView.mTvWorkerItemAge.visibility = View.GONE
        }

        // Load image inside view
        imageLoader.loadImage(worker.avatarUrl).apply(itemView.mIvWorkerItemPhoto, R.drawable.placeholder_face)

        // Invoke click delegate
        itemView.setOnClickListener { onClick(worker) }
    }

}