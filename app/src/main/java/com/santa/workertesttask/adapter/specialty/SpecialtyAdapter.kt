package com.santa.workertesttask.adapter.specialty

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.santa.workertesttask.model.specialty.Specialty
import com.santa.workertesttask.util.helper.Fonts
import com.santa.workertesttask.util.image.ImageLoader
import com.santa.workertesttask.util.standard.applyTypeface
import com.testtask.santa.workertesttask.R
import kotlinx.android.synthetic.main.item_specialty.view.*


class SpecialtyAdapter(
        listSpecialty: List<Specialty>,
        private val imageLoader: ImageLoader
) : RecyclerView.Adapter<SpecialtyViewHolder>() {

    private var mListSpecialty: List<Specialty> = listSpecialty

    var onClickAction: (Specialty) -> Unit = {}


    override fun getItemCount(): Int = mListSpecialty.count()


    override fun onBindViewHolder(holder: SpecialtyViewHolder?, position: Int) {
        holder?.bindView(mListSpecialty[holder.adapterPosition], onClickAction)
    }


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): SpecialtyViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_specialty, parent, false)
        return SpecialtyViewHolder(view, imageLoader)
    }


}


class SpecialtyViewHolder(
        itemView: View,
        private val imageLoader: ImageLoader
): RecyclerView.ViewHolder(itemView) {

    fun bindView(specialty: Specialty, onClick: (Specialty) -> Unit = {}) {
        // Setting specialty name
        itemView.mTvSpecialtyName.text = specialty.name
        itemView.mTvSpecialtyName.applyTypeface(Fonts.ROBOTO_LIGHT)

        // Setting image
        imageLoader.loadImage(R.drawable.placeholder_face)
                .apply(itemView.mIvSpecialtyImage, R.drawable.placeholder_face)

        // Invoke click delegate
        itemView.setOnClickListener { onClick(specialty) }
    }

}
