package com.santa.workertesttask.util.helper

/**
 * Created by santa on 20.12.2017.
 */

class GuardHelper(var message: String? = null) {

    fun getWithThrow(): String = throw IllegalStateException("Test exception")
    fun getWithSuccess(): String = message ?: "message"

}