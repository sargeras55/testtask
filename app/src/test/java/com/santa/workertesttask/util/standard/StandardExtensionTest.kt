package com.santa.workertesttask.util.standard

import com.santa.workertesttask.util.helper.GuardHelper
import org.junit.Assert.*
import org.junit.Test

/**
 * Created by santa on 20.12.2017.
 */


class StandardExtensionTest {


    /**
     * Test [guard]
     */
    @Test
    fun testGuardWithThrow() {
        val successMessage = "successMessage"
        val catchMessage = "catchMessage"
        val guardHelper = GuardHelper(successMessage)

        val message = guardHelper.guard(onTry = {
            it.getWithThrow()
        }, onCatch = {
            catchMessage
        })

        assertEquals("Message must be equal catchMessage", message, catchMessage)
        assertNotEquals("Message not must be equal successMessage", message, successMessage)
    }


    /**
     * Test [guard]
     */
    @Test
    fun testGuardWithSuccess() {
        val successMessage = "successMessage"
        val catchMessage = "catchMessage"
        val guardHelper = GuardHelper(successMessage)

        val message = guardHelper.guard(onTry = {
            it.getWithSuccess()
        }, onCatch = {
            catchMessage
        })

        assertEquals("Message must be equal successMessage", message, successMessage)
        assertNotEquals("Message not must be equal catchMessage", message, catchMessage)
    }


    /**
     * Test [guard]
     */
    @Test
    fun testGuardWithNull() {
        val catchMessage = "catchMessage"
        val guardHelper: GuardHelper? = null

        val message = guardHelper.guard(onTry = {
            it?.getWithSuccess()
        }, onCatch = {
            catchMessage
        })

        assertNull("Message must be null", message)
    }


    /**
     * Test [guardWith]
     */
    @Test
    fun testGuardWithWithSuccess() {
        val successMessage = "successMessage"
        val catchMessage = "catchMessage"
        val guardHelper = GuardHelper(successMessage)

        val message = guardWith(onTry = {
            guardHelper.getWithSuccess()
        }, onCatch = {
            catchMessage
        })

        assertEquals("Message must be equal successMessage", message, successMessage)
        assertNotEquals("Message not must be equal catchMessage", message, catchMessage)
    }


    /**
     * Test [guardWith]
     */
    @Test
    fun testGuardWithWithCatch() {
        val successMessage = "successMessage"
        val catchMessage = "catchMessage"
        val guardHelper = GuardHelper(successMessage)

        val message = guardWith(onTry = {
            guardHelper.getWithThrow()
        }, onCatch = {
            catchMessage
        })

        assertEquals("Message must be equal catchMessage", message, catchMessage)
        assertNotEquals("Message not must be equal successMessage", message, successMessage)
    }



    /**
     * Test [guardWith]
     */
    @Test
    fun testGuardWithWithNullCatch() {
        val nullMessage = null
        val guardHelper = GuardHelper(nullMessage)

        val message = guardWith(onTry = {
            guardHelper.getWithThrow()
        }, onCatch = {
            nullMessage
        })

        assertNull("Message must be null", message)
    }


}