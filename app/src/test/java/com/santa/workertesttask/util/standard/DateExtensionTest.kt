package com.santa.workertesttask.util.standard

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.nullValue
import org.junit.Assert
import org.junit.Test
import java.text.SimpleDateFormat


class DateExtensionTest {


    /**
     * Test [getDifferenceInYear] on 1 years
     */
    @Test
    fun testGetDifferenceInYearOne() {
        val formatDmy = SimpleDateFormat("dd.MM.yyyy")
        val fromLabel = "01.01.1993"
        val toLabel = "01.01.1994"

        val dateFrom = formatDmy.parse(fromLabel)
        val dateTo = formatDmy.parse(toLabel)

        val year = dateFrom.getDifferenceInYear(dateTo)

        Assert.assertEquals("Year must be 1", year, 1)
        Assert.assertNotEquals("Year not must be 0", year, 0)
    }


    /**
     * Test [getDifferenceInYear] on 0 years
     */
    @Test
    fun testGetDifferenceInYearZero() {
        val formatDmy = SimpleDateFormat("dd.MM.yyyy")
        val fromLabel = "01.01.1993"
        val toLabel = "31.12.1993"

        val dateFrom = formatDmy.parse(fromLabel)
        val dateTo = formatDmy.parse(toLabel)

        val year = dateFrom.getDifferenceInYear(dateTo)

        Assert.assertEquals("Year must be 0", year, 0)
        Assert.assertNotEquals("Year not must be 1", year, 1)
    }


    /**
     * Test [getDifferenceInYear] on 5 years
     */
    @Test
    fun testGetDifferenceInYearFive() {
        val formatDmy = SimpleDateFormat("dd.MM.yyyy")
        val fromLabel = "01.01.1993"
        val toLabel = "31.12.1998"

        val dateFrom = formatDmy.parse(fromLabel)
        val dateTo = formatDmy.parse(toLabel)

        val year = dateFrom.getDifferenceInYear(dateTo)

        Assert.assertEquals("Year must be 5", year, 5)
        Assert.assertNotEquals("Year not must be 6", year, 6)
    }


    /**
     * Test [transformDateFormat]
     */
    @Test
    fun testTransformDateFormatThreeSuccess() {
        val formatDmy = SimpleDateFormat("dd.MM.yyyy")
        val formatDmy1 = SimpleDateFormat("dd/MM/yyyy")
        val formatYmd = SimpleDateFormat("yyyy.MM.dd")
        val fromLabel = "01.01.1993"

        val dateFrom = transformDateFormat(fromLabel, formatDmy, formatDmy1, formatYmd)
        val dateSuccess = formatDmy.parse(fromLabel)

        Assert.assertEquals("Date must be equal to dateSuccess", dateFrom, dateSuccess)
    }


    /**
     * Test [transformDateFormat]
     */
    @Test
    fun testTransformDateFormatEmpty() {
        val fromLabel = "01.01.1993"
        val dateFrom = transformDateFormat(fromLabel)

        assertThat("Date must be null", dateFrom, `is`(nullValue()))
    }


    /**
     * Test [transformDateFormat]
     */
    @Test
    fun testTransformDateFormatNotFound() {
        val formatDmy = SimpleDateFormat("dd/MM/yyyy")
        val formatYmd = SimpleDateFormat("yyyy.MM.dd")
        val fromLabel = "01.01.1993"
        val dateFrom = transformDateFormat(fromLabel, formatDmy, formatYmd)

        assertThat("Date must be null", dateFrom, `is`(nullValue()))
    }

}