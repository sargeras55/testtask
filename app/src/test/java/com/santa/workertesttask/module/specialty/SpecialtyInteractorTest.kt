package com.santa.workertesttask.module.specialty

import android.os.Build
import com.azume.santa.countrytest.app.TestAppDelegate
import com.santa.workertesttask.model.join.company.WorkerSpecialty
import com.santa.workertesttask.model.join.company.WorkerSpecialtyRepository
import com.santa.workertesttask.model.specialty.Specialty
import com.santa.workertesttask.model.specialty.SpecialtyRepository
import com.santa.workertesttask.model.worker.Worker
import com.santa.workertesttask.model.worker.WorkerRepository
import com.santa.workertesttask.module.specialty.interactor.SpecialtyInteractor
import com.santa.workertesttask.module.specialty.interactor.SpecialtyInteractorReactive
import com.santa.workertesttask.network.adapter.worker.WorkersWrapper
import com.santa.workertesttask.network.service.WorkerService
import com.testtask.santa.workertesttask.BuildConfig
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.mockito.BDDMockito.*
import java.lang.NullPointerException

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, application = TestAppDelegate::class, sdk = [(Build.VERSION_CODES.LOLLIPOP)])
class SpecialtyInteractorTest {

    @Mock private lateinit var workerService: WorkerService

    @Mock private lateinit var workerRepository: WorkerRepository
    @Mock private lateinit var specialtyRepository: SpecialtyRepository
    @Mock private lateinit var companyRepository: WorkerSpecialtyRepository

    private lateinit var specialtyInteractor: SpecialtyInteractor


    // Specialties
    private val specialtiesEmpty = emptyList<Specialty>()
    private val specialties = listOf(
            Specialty(10, "Менеджер"),
            Specialty(20, "Программист"),
            Specialty(30, "HR"),
            Specialty(40, "CEO")
    )


    // Workers
    private val workersEmpty = emptyList<Worker>()
    private val workers = listOf(
            Worker(firstName = "Andrew", lastName = "Chupin"),
            Worker(firstName = "John", lastName = "Whilliams"),
            Worker(firstName = "Richard", lastName = "Anderson"),
            Worker(firstName = "Roman", lastName = "Smith")
    )


    // Companies
    private val companiesEmpty = emptyList<WorkerSpecialty>()
    private val companies = specialties.zip(workers, { specialty, worker ->
        WorkerSpecialty(worker = worker, specialty = specialty)
    })


    // Response Wrapper
    private val responseCompanyEmpty by lazy {
        WorkersWrapper(workers = workersEmpty, specialty = specialtiesEmpty, workerSpecialty = companiesEmpty)
    }

    private val responseCompany by lazy {
        WorkersWrapper(workers = workers, specialty = specialties, workerSpecialty = companies)
    }


    @Before
    fun prepareCountryTest() {
        MockitoAnnotations.initMocks(this)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler({ Schedulers.trampoline() })
        specialtyInteractor = SpecialtyInteractorReactive(specialtyRepository, workerRepository, companyRepository, workerService)
    }


    @Test
    fun testFindSpecialtiesNotEmpty() {
        // given
        given(specialtyRepository.findAll()).willReturn(specialties)


        // when
        val testObserver = specialtyInteractor.findSpecialties().test()
        testObserver.awaitTerminalEvent()

        // then
        testObserver.assertNoErrors()
                .assertComplete()
                .assertValue { list -> list.count() == specialties.count() }
                .assertValue { list -> list.contains(specialties[0]) }
    }


    @Test
    fun testFindSpecialtiesEmpty() {
        // given
        given(specialtyRepository.findAll()).willReturn(specialtiesEmpty)


        // when
        val testObserver = specialtyInteractor.findSpecialties().test()
        testObserver.awaitTerminalEvent()


        // then
        testObserver.assertNoErrors()
                .assertComplete()
                .assertValue { list -> list.count() == 0 }

        assertThat(testObserver.values(), not(hasItem(specialties[0])))
    }

    @Test
    fun testFindSpecialtiesNull() {
        // given
        given(specialtyRepository.findAll()).willThrow(NullPointerException())


        // when
        val testObserver = specialtyInteractor.findSpecialties().test()
        testObserver.awaitTerminalEvent()


        // then
        testObserver.assertNotComplete()
                .assertError(NullPointerException::class.java)
    }


    @Test
    fun testUpdateSpecialtiesNotEmpty() {
        // given
        given(specialtyRepository.findAll()).willReturn(specialties)
        given(workerService.loadWorkers()).willReturn(Single.just(responseCompany))

        // when
        val testObserver = specialtyInteractor.updateSpecialties().test()
        testObserver.awaitTerminalEvent()


        // then
        then(specialtyRepository).should(timeout(100)).saveCollection(responseCompany.specialty)
        then(workerRepository).should(timeout(100)).saveCollection(responseCompany.workers)
        then(companyRepository).should(timeout(100)).saveCollection(responseCompany.workerSpecialty)

        // then
        testObserver.assertNoErrors()
                .assertComplete()
                .assertValue { list -> list.count() == specialties.count() }
                .assertValue { list -> list.contains(specialties[0]) }
    }


    @Test
    fun testUpdateSpecialtiesEmpty() {
        // given
        given(specialtyRepository.findAll()).willReturn(specialtiesEmpty)
        given(workerService.loadWorkers()).willReturn(Single.just(responseCompanyEmpty))

        // when
        val testObserver = specialtyInteractor.updateSpecialties().test()
        testObserver.awaitTerminalEvent()


        // then
        then(specialtyRepository).should(timeout(100)).saveCollection(responseCompanyEmpty.specialty)
        then(workerRepository).should(timeout(100)).saveCollection(responseCompanyEmpty.workers)
        then(companyRepository).should(timeout(100)).saveCollection(responseCompanyEmpty.workerSpecialty)

        testObserver.assertNoErrors()
                .assertComplete()
                .assertValue { list -> list.count() == 0 }
    }


    @Test
    fun testUpdateSpecialtiesError() {
        // given
        given(workerService.loadWorkers()).willReturn(Single.error(NullPointerException()))

        // when
        val testObserver = specialtyInteractor.updateSpecialties().test()
        testObserver.awaitTerminalEvent()

        // then
        then(specialtyRepository).shouldHaveZeroInteractions()
        then(workerRepository).shouldHaveZeroInteractions()
        then(companyRepository).shouldHaveZeroInteractions()

        testObserver.assertNotComplete()
                .assertError(NullPointerException::class.java)
    }


    @After
    fun finalizeCountryTest() {
        Mockito.reset(workerService, specialtyRepository, workerRepository, companyRepository)
        RxAndroidPlugins.reset()
    }

}