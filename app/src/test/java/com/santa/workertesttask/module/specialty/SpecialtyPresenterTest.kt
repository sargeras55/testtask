package com.santa.workertesttask.module.specialty

import android.os.Build
import com.azume.santa.countrytest.app.TestAppDelegate
import com.santa.workertesttask.model.specialty.Specialty
import com.santa.workertesttask.module.specialty.contract.SpecialtyPresenter
import com.santa.workertesttask.module.specialty.contract.SpecialtyView
import com.santa.workertesttask.module.specialty.interactor.SpecialtyInteractor
import com.santa.workertesttask.module.specialty.router.SpecialtyRouter
import com.testtask.santa.workertesttask.BuildConfig
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.then
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.timeout
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.lang.NullPointerException


@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, application = TestAppDelegate::class, sdk = [(Build.VERSION_CODES.LOLLIPOP)])
class SpecialtyPresenterTest {

    @Mock private lateinit var specialtyView: SpecialtyView
    @Mock private lateinit var specialtyInteractor: SpecialtyInteractor
    @Mock private lateinit var specialtyRouter: SpecialtyRouter
    private lateinit var specialtyPresenter: SpecialtyPresenter


    private val specialtiesEmpty = emptyList<Specialty>()
    private val specialties = listOf(
            Specialty(0, "Менеджер"),
            Specialty(0, "Программист"),
            Specialty(0, "HR"),
            Specialty(0, "CEO")
    )



    @Before
    fun prepareCountryTest() {
        MockitoAnnotations.initMocks(this)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler({ Schedulers.trampoline() })
        specialtyPresenter = SpecialtyPresenter(specialtyInteractor, specialtyView, specialtyRouter)
    }


    @Test
    fun testOnTakeSpecialtiesActionExist() {
        // given
        given(specialtyInteractor.findSpecialties()).willReturn(Single.just(specialties))

        // when
        specialtyPresenter.onTakeSpecialtiesAction()

        // than
        then(specialtyRouter).should(timeout(100)).navigateToListSpecialties()
        then(specialtyView).should(timeout(100)).showSpecialties(specialties)
    }


    @Test
    fun testOnTakeSpecialtiesActionEmptyAndEmpty() {
        // given
        given(specialtyInteractor.findSpecialties()).willReturn(Single.just(specialtiesEmpty))
        given(specialtyInteractor.updateSpecialties()).willReturn(Single.just(specialtiesEmpty))

        // when
        specialtyPresenter.onTakeSpecialtiesAction()

        // than
        then(specialtyRouter).should(timeout(100)).navigateToLoading()
        then(specialtyRouter).should(timeout(100)).navigateToEmpty()
    }


    @Test
    fun testOnTakeSpecialtiesActionEmptyAndExist() {
        // given
        given(specialtyInteractor.findSpecialties()).willReturn(Single.just(specialtiesEmpty))
        given(specialtyInteractor.updateSpecialties()).willReturn(Single.just(specialties))

        // when
        specialtyPresenter.onTakeSpecialtiesAction()

        // than
        then(specialtyRouter).should(timeout(100)).navigateToLoading()
        then(specialtyRouter).should(timeout(100)).navigateToListSpecialties()
        then(specialtyView).should(timeout(100)).showSpecialties(specialties)
    }


    @Test
    fun testOnTakeSpecialtiesActionThrow() {
        // given
        given(specialtyInteractor.findSpecialties()).willReturn(Single.error(NullPointerException("Test null")))

        // when
        specialtyPresenter.onTakeSpecialtiesAction()

        // than
        then(specialtyRouter).should(timeout(100)).navigateToEmpty()
    }


    @Test
    fun testOnTakeSpecialtiesActionEmptyAndThrow() {
        // given
        given(specialtyInteractor.findSpecialties()).willReturn(Single.just(specialtiesEmpty))
        given(specialtyInteractor.updateSpecialties()).willReturn(Single.error { throw java.lang.NullPointerException("Country list is null") })

        // when
        specialtyPresenter.onTakeSpecialtiesAction()

        // than
        then(specialtyRouter).should(timeout(100)).navigateToLoading()
        then(specialtyRouter).should(timeout(100)).navigateToNetworkError()
    }



    @After
    fun finalizeCountryTest() {
        Mockito.reset(specialtyView, specialtyInteractor, specialtyRouter)
        RxAndroidPlugins.reset()
    }

}