package com.santa.workertesttask.module.workers

import android.os.Build
import com.azume.santa.countrytest.app.TestAppDelegate
import com.santa.workertesttask.model.specialty.Specialty
import com.santa.workertesttask.model.worker.Worker
import com.santa.workertesttask.module.workers.contract.WorkersAction
import com.santa.workertesttask.module.workers.contract.WorkersPresenter
import com.santa.workertesttask.module.workers.contract.WorkersView
import com.santa.workertesttask.module.workers.interactor.WorkersInteractor
import com.santa.workertesttask.module.workers.router.WorkersRouter
import com.testtask.santa.workertesttask.BuildConfig
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config


@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, application = TestAppDelegate::class, sdk = [(Build.VERSION_CODES.LOLLIPOP)])
class WorkersPresenterTest {

    @Mock private lateinit var workersView: WorkersView
    @Mock private lateinit var workersRouter: WorkersRouter
    @Mock private lateinit var workersInteractor: WorkersInteractor
    private lateinit var workersAction: WorkersAction

    private val specialty = Specialty(0, "CEO")
    private val worker = Worker()

    private val workersEmpty = emptyList<Worker>()
    private val workers = listOf(
            Worker(),
            Worker(),
            Worker(),
            Worker(),
            Worker()
    )

    @Before
    fun prepareCountryTest() {
        MockitoAnnotations.initMocks(this)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler({ Schedulers.trampoline() })
        workersAction = WorkersPresenter(workersView, workersRouter, workersInteractor)
    }


    @Test
    fun testOnTaleWorkersActionExist() {
        // given
        BDDMockito.given(workersInteractor.findWorkersBySpecialtyId(specialty.specialtyId)).willReturn(Single.just(workers))

        // when
        workersAction.onTaleWorkersAction(specialty)

        // than
        BDDMockito.then(workersRouter).should(Mockito.timeout(100)).navigateToWorkerList()
        BDDMockito.then(workersView).should(Mockito.timeout(100)).showWorkers(workers)
    }


    @Test
    fun testOnTaleWorkersActionEmpty() {
        // given
        BDDMockito.given(workersInteractor.findWorkersBySpecialtyId(specialty.specialtyId)).willReturn(Single.just(workersEmpty))

        // when
        workersAction.onTaleWorkersAction(specialty)

        // than
        BDDMockito.then(workersRouter).should(Mockito.timeout(100)).navigateToEmpty()
    }


    @Test
    fun testOnShowWorkerAction() {
        // given

        // when
        workersAction.onShowWorkerAction(worker)

        // than
        BDDMockito.then(workersRouter).should(Mockito.timeout(100)).navigateToWorker(worker)
    }


    @After
    fun finalizeCountryTest() {
        Mockito.reset(workersView, workersRouter, workersInteractor)
    }

}