package com.azume.santa.countrytest.app

import android.app.Application
import com.activeandroid.ActiveAndroid

/**
 * Created by santa on 12.12.2017.
 */

class TestAppDelegate: Application() {

    override fun onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
    }

}
